variable "ami-linux" {
  description = "image version"
  default = "ami-0dacb0c129b49f529"
  type = string
}

variable "ssh_key_name" {
  default = "kuzmich"
  description = "name for ssh key"
  type = string
}

variable "ssh_key_path" {
  description = "path to ssh key for provision"
  type = string
  default = "~/.ssh/kuzmich.pem"
}