terraform {
  backend "s3" {
    bucket         = "terraform-state-lite"
    key            = "global/app/terraform.tfstate"
    region         = "us-east-2"
    dynamodb_table = "terraform-locks"
    encrypt        = true
  }
}

provider "aws" {
  region = "us-east-2"
}

data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "all" {
  vpc_id = data.aws_vpc.default.id
}

module "security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 3.0"

  name        = "app"
  description = "Security group for example usage with EC2 instance"
  vpc_id      = data.aws_vpc.default.id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["http-80-tcp", "ssh-tcp" ]
  egress_rules        = ["all-all"]
}

resource "aws_network_interface" "this" {
  count = 1

  subnet_id = tolist(data.aws_subnet_ids.all.ids)[count.index]
}

module "ec2" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "~> 2.0"

  instance_count = 1
  name          = "test-app"
  ami           = var.ami-linux

  instance_type = "t3.nano"
  subnet_id     = tolist(data.aws_subnet_ids.all.ids)[0]
  vpc_security_group_ids      = [module.security_group.this_security_group_id]
  associate_public_ip_address = true
  key_name               = var.ssh_key_name

  root_block_device = [
    {
      volume_type = "gp2"
      volume_size = 10
    },
  ]

  tags = {
    "Env"      = "Live"
    "Location" = "Secret"
  }
}

resource "null_resource" "install-docker" {
  triggers = {
    cluster_instance_ids = join(",", module.ec2.id)
  }
  connection {
    host        = join(",", module.ec2.public_ip)
    user        = "ec2-user"
    private_key = file(var.ssh_key_path)
    agent       = true
    timeout     = "3m"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo yum update -y",
      "sudo amazon-linux-extras install -y docker",
      "sudo service docker start",
      "sudo usermod -a -G docker ec2-user",
      "sudo systemctl enable docker",
      "sudo curl -L 'https://github.com/docker/compose/releases/download/1.25.3/docker-compose-Linux-x86_64' -o /usr/local/bin/docker-compose",
      "sudo  chmod +x /usr/local/bin/docker-compose",
      "sudo mkdir /app",
      "sudo chown ec2-user: /app",
      "sudo docker info"
    ]
  }
}